import { Component } from '@angular/core';

@Component({
  selector: 'app-home-one',
  templateUrl: './home-one.component.html',
  styleUrls: ['./home-one.component.css']
})

export class HomeOneComponent {
  pageText = {
    title: 'AYL STUDIO',
    name: 'ALEX LI',
    email: 'ALEXYUKEWLI@GMAIL.COM',
    description: 'I am a UI/UX developer. Always looking to learn more and meet new people.'
  };

  onIconClick (type) {
    if (type === 'instagram') {
      window.open('https://www.instagram.com/aylstudio/');
    } else if (type === 'gitlab') {
      window.open('https://gitlab.com/alexyli');
    } else if (type === 'linkedin') {
      window.open('https://www.linkedin.com/in/alex-li-346016ab/');
    }
  }
}
